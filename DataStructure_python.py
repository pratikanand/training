# data structure of python 
# 1) list :  
#			a) append(x) same as a[len(a):] = [x] 
#			b) extend(iterable) same as a[len(a):] = iterable
#			c) insert(i,x)  
#			d) remove(x) 
#			e) pop(i) 
#			f) clear()
#			g) count(x)
#			h) sort(), reverse()

# list as stack using append & pop
# list as queue using -- from collections import deque and append() & popleft()

o = [x**2 for x in range(10)]
print(o)
o = list(map(lambda x: x**3, range(10)))
print(o)

o = [(x,y) for x in range(3) for y in range(3) if x != y]
print(o)

fruit = ['  banana', '  loganberry ', 'passion fruit  ']
o = [(word.strip(), word.upper()) for word in fruit]
print(o)

matrix = [[1, 2, 3, 4],[5, 6, 7, 8],[9, 10, 11, 12]]
o = [[row[i] for row in matrix] for i in range(4)]
print(o)

o = []
for i in range(4):
	temp = []
	for row in matrix:
		temp.append(row[i])

	o.append(temp)

print(o)

print()
print(list(zip(*matrix)))

a = [1,2]
b = [3,4]
o = list(zip(a,b))
print(o)

o = [(x+y+z) for x,y,z in zip(*matrix)]
print(o)

# tuple 
t = 1, 5, 'hi!'
print(t)
t = 1, 
print(t)   # "," is necessary

#sets
# unordered & no duplicate element
s = set('apple')
print(s)
s= {'apple', 'orange',1}
print(s)

s = {x for x in 'abracadabra' if x not in 'abc'}
print(s)

# dict
# list(d) list of keys, sorted(d), "in" to check is key in dict same with "not in"
d = {'sepal': 2, 'petal': 4}
print(list(d))
s ='petal' in d
print(s)

d = {'city': 'Pune', 'rating': 5}
for k,v in d.items():
	print(k,v)



