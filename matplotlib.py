# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 01:27:11 2021

@author: anand
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import random
from numpy import exp

# visualing topology of model

X_val = []
random.seed(5)
for i in range(2000):
    num = random.uniform(-5,5)
    X_val.append(num)    
X_val = np.array(X_val)

X_val = np.arange(-5, 5, 0.0125)

Y_val = []    
random.seed(10)
for i in range(2000):
    num = random.uniform(-5,5)  
    Y_val.append(num)
Y_val = np.array(Y_val)

Y_val = np.arange(-5, 5, 0.0125)

X_val, Y_val = np.meshgrid(X_val, Y_val)

# ------------------------------------------------------------------------------- #
Z_val = []
for x,y in zip(X_val,Y_val):
    
    z1 = 0.02*x + 0.5*y + 0.9
    z2 = 1 / (1 + exp(-z1))
    
    z3 = 0.2*x + 1.9*y + 0.66
    z4 = 1 / (1 + exp(-z3))
    
    z5 = 1.4*x + 0.122*y + 1.99
    z6 = 1 / (1 + exp(-z5))
    
    z7 = 0.88*z2 + 2.11*z4 + 0.11*z6
    z8 = 1 / (1 + exp(-z7))
    
    z9 = 0.1*z2 + 0.22*z4 + 0.33*z6
    z10 = 1 / (1 + exp(-z9))
    
    z11 = 0.5 * (1 - z8)**2
    z12 = 0.5 * (0 - z10)**2
    
    z13 = z11 + z12
    
    Z_val.append(z13)
    
Z_val = np.array(Z_val)
fig = plt.figure()
ax = fig.gca(projection='3d')

#tri_surf = Axes3D.plot_trisurf(X_val,Y_val,Z_val)

surf = ax.plot_surface(X_val,Y_val,Z_val, cmap= cm.coolwarm,
                       linewidth=0, antialiased=False)

# Customize the z axis.
#ax.set_zlim(-1.01, 1.01)
#ax.zaxis.set_major_locator(LinearLocator(10))
#ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

# Add a color bar which maps values to colors.
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.show()

data = pd.read_csv('D:/Data/breastcancer.csv')

x = np.random.randint(1,10,10)
y1 = np.random.normal(5,1,10)
y2 = np.random.randint(1,10,10)

plt.figure(figsize= (5,5), dpi =50)
plt.plot(x,y1, label = 'normal', color = 'yellow')
plt.plot(x,y2, label = 'uniform', color ='green')
plt.title('normal_random', fontdict={'fontname':'Comic Sans MS', 'fontsize': 20})
plt.title('new plot')
plt.xlabel('x axis')
plt.ylabel('y axis')
plt.legend()
plt.savefig('myplot.png', dpi= 200)
plt.show()


plt.plot([1, 2, 3], label='Line 2')
plt.plot([3, 2, 1], label='Line 1')
plt.legend()
# or this way
plt.legend(handles=[line_up, line_down])



