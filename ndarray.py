# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 00:04:22 2021

@author: anand
"""

import numpy as np 

D_array = np.array([[1,2],[3,4]])

T_array = np.array([[[1,2],[3,4]],[[11,22],[33,44]],[[1,2],[3,4]]])

# to get number of axes 
D_array.ndim
T_array.ndim

D_array.shape
D_array.size # to get number of elements 

a = np.arange(15, dtype= complex).reshape(3,5)

np.zeros((3,5))
np.ones((3,5))
np.empty((2,3))

np.arange(0,10,2)
from numpy import pi
np.linspace(0,20,10000)

import sys
np.set_printoptions(threshold=sys.maxsize)
np.linspace(0,20,10000)

A = np.array([[1,2],[3,4]])
B = np.array([[11,22],[33,44]])
A+B
np.add(A,B)

A*B # elementwiase product
A@B # matrix product
A.dot(B)
A.sum()
A.min()
A.max()

# 0 means 'for loop' over 'rows' && 1 means 'for loop' over 'columns'
A.sum(axis = 0)
A.sum(axis = 1)
A.cumsum(axis = 0)

a = np.arange(10)**3
a
a[0:6:2]
a[: : -1]

def f(x,y):
    return 10*(x+y)
b = np.fromfunction(f,(5,4),dtype=int)
b

for i in A:
    print(i)

for i in A.ravel():
    print(i)

A.ravel()
A.reshape(4,)   # it return the reshaped array 
A.T

A.resize(4,)    # it actally the reshape the array itself

b.reshape(2,-1) # -1 automatically calculate other dimensions.  

A.resize((2,2))
np.vstack((A,B))
np.concatenate((A,B),axis=0)
np.hstack((A,B))
np.concatenate((A,B),axis=1)

np.all(A)   # return True when all element is not equal to 0
np.any(A)   # Test whether any array element along a given axis evaluates to True

#    if condition is true then yield x, otherwise yield y
np.where([[True, False], [False, True]],
[[1, 2], [3, 4]],
[[9, 8], [7, 6]])

a = np.arange(10)
np.where(a < 5,a ,10*a)

a = np.arange(9).reshape(3,3) + 10
np.argmax(a,axis=0)



