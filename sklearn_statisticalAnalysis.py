# -*- coding: utf-8 -*-
"""
Created on Thu Jan  7 10:17:21 2021

@author: anand
"""

%matplotlib inline
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn import svm
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.model_selection import train_test_split

b_c = pd.read_csv('D:/Data/breastcancer.csv')

b_c.head()
b_c.shape
b_c.info()

sam = pd.read_csv('H:/Praik_ubuntu/download/loanpredictionAnalyticVidhya/train_u6lujuX_CVtuZ9i.csv')
# .info() gives info about how many non-null value in the columns. 

b_c.isnull().sum()
b_c[b_c.isnull().any(axis =1)]
sam.isnull().sum()

bins = (2,6.5,8)
group_names = ['bad', 'good']


from random import random
random()

from random import randint
randint(1,10)

from random import choice
a = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'] #list
choice(a)

from random import sample
sample(a, 4) # return list of 4 item randomly from "a"

from random import shuffle
shuffle(a)

import numpy as np
np.random.rand(5)

np.random.randint(2,20, 5)

np.random.randn(10)
np.random.shuffle(a)

pd.cut(np.random.rand(10), bins = (0,1), labels=['P'])

label_encode = LabelEncoder()
b_c['diagnosis'] = label_encode.fit_transform(b_c['diagnosis'])
b_c['diagnosis'].unique()

b_c['diagnosis'].value_counts()
sns.countplot(b_c['diagnosis'])


X = b_c.drop('diagnosis', axis = 1)
y = b_c['diagnosis']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state= 42)

sc = StandardScaler()
X_train = sc.fit_transform(X_train)     # fit and then transform 
X_test = sc.transform(X_test)           # transform with the firstly calculated value of mean and standard deviation

rfc = RandomForestClassifier(n_estimators=200)
rfc.fit(X_train, y_train)
pred_rfc = rfc.predict(X_test)

print(classification_report(y_test, pred_rfc))
print(confusion_matrix(y_test, pred_rfc))

clf = SVC()
clf.fit(X_train,y_train)
pred_clf = clf.predict(X_test)
print(classification_report(y_test, pred_clf))
print(confusion_matrix(y_test, pred_clf))

mlp = MLPClassifier(hidden_layer_sizes=(30,30,20), max_iter = 500)
mlp.fit(X_train, y_train)
pred_mlp = mlp.predict(X_test)
print(classification_report(y_test, pred_mlp))
print(confusion_matrix(y_test, pred_mlp))
cm = accuracy_score(y_test, pred_mlp)   # precision



#-----------------------------------------------------------------------------#
import pandas as pd 
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

import numpy as np
from sklearn.naive_bayes import BernoulliNB
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report


from sklearn.datasets import load_boston
boston = load_boston()
boston

df_X = pd.DataFrame(boston.data, columns = boston.feature_names)
df_y = pd.DataFrame(boston.target)
df_X.describe()
from sklearn.linear_model import LinearRegression
X_train, X_test, y_train, y_test = train_test_split(df_X, df_y, test_size = 0.2, random_state = 42)

reg = LinearRegression()
reg.fit(X_train, y_train)

reg.coef_

for i in {'kk':66, 'h' : 5}:
    print(i)
    
def ch():
    s = []
    for x in range(10):
        s.append(x**2)
        
    return s

[(x,y) for x in range(3) for y in range(5,8)]


###############################################################################
# sklearn Pipeline

# List of transforms and the final estimater. The purpose is to cross validate 
# this list of transformations. 

from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA

X, y = make_classification(random_state=0)
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
pipe = Pipeline([('scaler', StandardScaler()), ('svc', SVC())])

pipe.fit(X_train, y_train)
pipe.score(X_test, y_test)

from sklearn.pipeline import make_pipeline
# this make_pipeline 
estimators = [('reduce_dim', PCA()), ('clf', SVC())]
pipe1 = Pipeline(estimators)

pipe2 = make_pipeline(PCA(), SVC())
# pipe1 & pipe2 have same return

pipe1[0]
pipe1.named_steps.reduce_dim
# both give same return

pipe1[:1]  # to get sub pipeline 

# set parameter of the estimater 
pipe1.set_params(clf__C = 10)

from sklearn.model_selection import GridSearchCV
param_grid_value = dict(reduce_dim__n_components = [2,5,10], clf__C = [0.1, 10, 100])

grid_search = GridSearchCV(pipe1, param_grid = param_grid_value)

------------------------------------------------------------------------------
# statistical analysis
from sklearn.datasets import load_breast_cancer

data = load_breast_cancer()
X = data.data
y = data.target

X.shape
X

import pandas as pd
import numpy as np
import seaborn as sns

breastcancer_data = pd.read_csv('D:/Data/breastcancer.csv')
colon_data = pd.read_csv('D:/Data/colon_labled.csv')
breastcancer_data.head()
breastcancer_data.info()
describ = breastcancer_data.describe()
breastcancer_data.isnull().sum()

pd.isna(breastcancer_data)
breastcancer_data.nunique()
correlation = breastcancer_data.corr() 
sns.heatmap(correlation, xticklabels = correlation.columns, yticklabels = 
            correlation.columns)
sns.pairplot(breastcancer_data)
sns.distplot(breastcancer_data['radius_mean'])
sns.distplot(breastcancer_data['symmetry_mean'])


Index(['radius_mean', 'texture_mean', 'perimeter_mean', 'area_mean',
       'smoothness_mean', 'compactness_mean', 'concavity_mean',
       'concave points_mean', 'symmetry_mean', 'fractal_dimension_mean',
       'radius_se', 'texture_se', 'perimeter_se', 'area_se', 'smoothness_se',
       'compactness_se', 'concavity_se', 'concave points_se', 'symmetry_se',
       'fractal_dimension_se', 'radius_worst', 'texture_worst',
       'perimeter_worst', 'area_worst', 'smoothness_worst',
       'compactness_worst', 'concavity_worst', 'concave points_worst',
       'symmetry_worst', 'fractal_dimension_worst', 'diagnosis'],
      dtype='object')
# EDA gives idea about outlier and on the relation between features.

# z test, t test


breastcancer_data['concavity_se'].mean()

import scipy.stats as st
sample_data = breastcancer_data['radius_mean'].sample(frac = 0.25) # or n= any number
mean = breastcancer_data['radius_mean'].mean()
st.ttest_1samp(sample_data, mean)

st.ttest_1samp(sample_data, mean+0.60)
# F test compare the variance

# to test distribution is normal or not
from scipy import stats
sample_data = breastcancer_data['symmetry_mean']
shapiro_test = stats.shapiro(sample_data)
print(shapiro_test)

sample_data = np.random.normal(0,5,100)
print(stats.shapiro(sample_data))

sample_data = np.random.poisson(0,100)
print(stats.shapiro(sample_data))

# to check QQ plot
from seaborn_qqplot import pplot
sns.distplot(breastcancer_data['radius_mean'])
sns.distplot(breastcancer_data['concave points_worst'])
# this plot already showed that theses two have normal distribution which is varified
# from the upcoming qq plot.
pplot(breastcancer_data, x = 'radius_mean', y ='concave points_worst', kind='qq')

pplot(breastcancer_data, x = 'radius_mean', y ='area_worst', kind='qq')


from scipy.stats import gamma
pplot(breastcancer_data, x = 'symmetry_mean', y = gamma, kind = 'qq')

# it is near to normal distribution
mean = breastcancer_data['symmetry_mean'].mean()
sample_data = breastcancer_data['symmetry_mean'].sample(frac = 0.01) # or n= any number
st.ttest_1samp(sample_data, mean+0.5)
# sample data and find p-value

# Tietjen-Moore test to check is any outlier or not
# Tietjen-Moore test [1]_ to detect multiple outliers in a univariate
# data set that follows an approximately normal distribution.

# i don't think this is the good way because it just remove the given number
# of expected outlier from the data set. 
import scikit_posthocs as sp
sp.outliers_tietjen(breastcancer_data['area_worst'].values, 10).ndim


# the significance of the correlation is checked by t-test.
# The null hypothesis of this test assumes that the correlation among
# the variables is not significant

from scipy.stats import pearsonr
stat, p = pearsonr(breastcancer_data['radius_mean'], breastcancer_data['symmetry_mean'])

# chi square test to check whether the frequency distribution are 
# significantly different from each other.
# The null hypotheses for the test is the two data sets are homogeneous.

from scipy.stats import chisquare
chisquare(breastcancer_data['radius_mean'], breastcancer_data['symmetry_mean'])
  



name = colon_data.columns
for i in range(15):
    r = np.random.randint(2000)
    n = colon_data.columns[r]
    sns.distplot(colon_data[n])
    















