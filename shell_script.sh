# "which bash" to get location of bash 

#! /bin/bash
echo "Hi Predera"

echo $BASH
echo $PWD
echo $HOME

best_programming_language = Python
echo best programming language $best_programming_language

echo "Enter your fav subject : "
read Subject_name

echo your fav subject is : $Subject_name  

read -p "Enter your fav algorithm : " fav_algorithm
read -sp "user password : " pass_word

echo favourit algorithm is $fav_algorithm
echo password $pass_word

echo "enter any number : "
read    # no variable given so store in "REPLY" inbuild variable
echo number is $REPLY

# passing argument from bash script
echo $1 $2 $3 ' > echo $1 $2 $3'

echo "Enter any integer : "
read var 
if (( $var > 0 )) 
then 
	echo 'positive integer $var'
fi

if [[ $var < 5 ]]; then
	echo "False"
else
	echo "true"
fi

# file exist or not
echo -p "Enter file name : " file_name

if [[ -e $file_name ]]; then
	echo "file found"
else
	echo "file not found"
fi

# -d for directory check
# -f for regular file check

# check permission with the given file

if [[ -e $file_name ]]
then
	if [[ -w $file_name ]]
	then
		echo "writing any text and then press ctrl+d to quit"
		cat >> file_name
	else
		echo "don't have write permission"
else
	echo "file not found"
fi

age = 27
if [[ $age > 18 && $age < 40 ]]
then
	echo "young"
else
	echo "old"
fi

# "||" for or operator

num1 = 23
num2 = 76

num_addition = $(( num1 + num2 ))
echo $num_addition

echo $(( num1 + num2 ))
echo $(( num1 - num2 ))
echo $(( num1 * num2 ))
echo $(( num1 / num2 ))
echo $(( num1 % num2 ))

# for decimal number
# "bc" command for arthimatic operation on decimal number

echo "20.4 + 0.55" | bc   # means "|" allow left side as input to right side command so here bc command have input from left
echo "scale=5;20.4 / 0.55" | bc

echo "scale=5; sqrt(33)" | bc -l


vehicle = $1 # first argument to this variable "vehicle"

case $vehicle in
	"car" )
		echo rent is rs 20 per km;;
	"truck" )
		echo rent is rs 10 per km;;
	* )
		echo vehicle is not available;;
esac

# pattern can be anythings  like here it is "car" , "truck" ,, can refer wiki page on "regular pattern"
# [a-z], [A,Z], [0-9], ?, *    "?" for single special charecter and "*" for random 

# while loop
n=1
while [[ n<=9 ]]; do
	echo $n
	n = $(( n + 1 )) 
done

# for loop
for (( i = 0; i < 10; i++ )); do
	echo $i
done

for i in {1..10}   # {start..end..increment}
do
	echo $i
done

#break and continue
#same 










































# Shell script is not have to compile, it is directly execute.
