import findspark
findspark.find()

findspark.init()
from pyspark import SparkContext as sc 

a = sc.parallelize([1,2,3])
a.collect()

# now a is RDD
type(a)

# better to use "take"
a.take(1)

# make pyspark dataframe which gets distributed into multiple nodes and we can run sql queries to get the data as per need

import findspark
findspark.init()
from pyspark.sql import SparkSession
spark = SparkSession.builder.master() 

df = spark.createDataFrame([{"predera": "hyderabad"} for x in range(100)])
df.show(5)

df = spark.read.format("csv").option("header", "true").option("inferSchema", "true").load("/home/panand/Downloads/CVs_HeLa_mk2.csv")
df.show()

df.createOrReplaceTempView("anyname")

spark.sql("select count(*) from  anyname").show()


# ------------------------------------------------------------------------------------------------------------------------------------ #
df = spark.read.format("csv").option("header", "true").option("inferSchema", "true").load("/home/panand/Downloads/CVs_HeLa_mk2.csv")
df.show(10)

df.createOrReplaceTempView("peptide_prediction")

spark.sql("select max(apex) from peptide_prediction").show()
spark.sql(" select count(*) from peptide_prediction").show()
spqrk.sql(" ")



