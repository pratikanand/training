# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 14:02:23 2021

@author: anand
"""

import pandas as pd

data = pd.read_csv('D:/Data/breastcancer.csv')
data.head()
data.tail()
data.info()
data.describe()

data.isnull().sum()
pd.isna(data)
data.columns

data['radius_mean'][1:10]
data.radius_mean

# pandas series
data['radius_mean'].size
data['radius_mean'].ndim
data['radius_mean'].is_monotonic
data['radius_mean'].name
data['radius_mean'].loc[data['radius_mean'] > 11.11]

data.iloc[:,:] # same as 'R'
data[['radius_mean','texture_mean','diagnosis']]

data.iloc[0,:]

# iterate over each columns of the data
for column_no, column in data.iteritems():
    print(column_no , column)
    
# iterate over each rows
for row_no, row in data.iterrows():
    print(row_no, row)
    
# find row with specific value in a particular columns 
data.loc[data['diagnosis'] == 'M']
# or
data.loc[data['radius_mean'] == 17.99]

data.sort_values('diagnosis')

# make new column
data['new_column'] = data['radius_mean'] + data['texture_mean'] + data['perimeter_mean']
data.shape
data = data.drop(['radius_mean', 'texture_mean', 'perimeter_mean'], axis = 1)
# data.drop([])
import random
states = random.choices(['sepal', 'petal', 'indrani', 'krishna'], k = 569)
data['state'] = states

data.loc[data['state'] == 'petal']
# but for more than one parameter , use this
data.loc[data['state'].isin(['petal','sepal'])]

# setting value at the specified location
data.at[0,'radius_mean'] = 2

# using position
data.iat[0,1] = 4

# drop row having missing data
data.dropna(how = 'any', inplace = True)

# fill the missing value with specified value
data.fillna(value = 5) 
# applying function to the data
data.apply(np.cumsum, axis = 0)  # 0 or 'index' means loop over row , 1 or 'columns' meansloop over column

data.apply(lambda x: x.max(), axis= 0)
# 
    
# rearrange columns of the data set
cols = list(data.columns)
new_cols = []
new_cols.extend(cols[0:27])
new_cols.append(cols[-1])
new_cols.append(cols[-2])
data = data[new_cols]

# or 
cols = list(data.columns)
cols = cols[0:27] + [cols[-1]] + [cols[-2]]
data = data[cols]

# save as CSV file
data.to_csv('modified_breast_cancer_data.csv')

# applying filter in the data
# very very import for "and" , "or" use "&" "|" only work  
data.loc[(data['diagnosis'] == 'M') & (data['new_column'] > 64)]
data['diagnosis'].str.containers('required_str')

data.loc[data['filter_column'] == '', ['any_column']] = 'update value'

# question by veena
data.loc[data['state'] == 'petal']['radius_mean'].sum()
# converting list to series
a = random.choices([1,2,3], k =500)
a = pd.Series(a)
a.sum()
# grouping

grouped = data.groupby('state') 
grouped.groups
sumed_value = grouped.sum()
sumed_value['radius_mean']
# understanding group by
df = pd.DataFrame(
    [
     ("bird", "Falconiformes", 389.0),
     ("bird", "Psittaciformes", 24.0),
     ("mammal", "Carnivora", 80.2)
     ],
    index=["falcon", "parrot", "lion"],
    columns=("class", "order", "max_speed")
    )
grouped = df.groupby('class')
grouped.sum()

from sklearn.datasets import load_breast_cancer


li = [5, 7, 22, 97, 54, 62, 77, 23, 73, 61]
list(filter(lambda x: (x > 10), li))

list(map(lambda x: (x*2), li))

for df in pd.read_csv('D:/Data/breastcancer.csv', chunksize= 5):
    print(df)

###############################################################################
#                 Pd.Series()

# lots of attributes and method
a = [1,2,3,4]
a = pd.Series(a)
a.is_unique
a.abs()
b = [-1,-2,-3,-4,1,2]
a.add(b)
